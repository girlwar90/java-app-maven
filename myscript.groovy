def buildJar(){
    echo "building the application"
    sh 'mvn package'
}

def buildAndPushImage(){
    echo "building image"
    withCredentials([usernamePassword(credentialsId: 'docker-hub-cred', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]){
        echo "username is $USERNAME"
        sh 'docker build -t 082013/demo-app:test-9.9 .'
        sh "echo $PASSWORD | docker login -u $USERNAME  --password-stdin"
        sh 'docker push 082013/demo-app:test-9.9'
    }
}

def deployApp(){
    echo "deploying application"
}

return this